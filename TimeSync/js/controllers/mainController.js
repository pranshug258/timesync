﻿(function () {
    'use strict';

    timeSync.controller("mainController", ['$scope', mainController]);

    var applicationData = Windows.Storage.ApplicationData.current;
    var localFolder = applicationData.localFolder;
    var saveData = function (obj) {
        return localFolder.createFileAsync("routine.json", Windows.Storage.CreationCollisionOption.replaceExisting).then(function (sampleFile) {
            return Windows.Storage.FileIO.writeTextAsync(sampleFile, obj.toString());
        })
    }

    function mainController($scope) {
        $scope.state = "home";
        $scope.routine = {};
        localFolder.getFileAsync("routine.json").then(function (file) {
            return Windows.Storage.FileIO.readTextAsync(file);
        }, function () {
            var r = JSON.parse('{ "Monday" : {"activities" : []},"Tuesday" : {"activities" : []},"Wednesday" : {"activities" : []},"Thursday" : {"activities" : []},"Friday" : {"activities" : []},"Saturday" : {"activities" : []},"Sunday" : {"activities" : []}}')
            var jsontext = JSON.stringify(r)
            //console.log(jsontext)
            saveData(jsontext);
        }).done(function (text) {
            if (text != null) {
                $scope.routine.jsonData = JSON.parse(text)
               //console.log($scope.routine)
            }
        });

    }

}());