﻿(function () {
    'use strict';

    timeSync.controller("homeController", ['$scope', 'homeManager', homeController]);

    function homeController($scope, homeManager) {
        $scope.day = homeManager.getDay();
        
    }

}());