﻿(function () {
    'use strict';

    timeSync.controller("routinesController", ['$scope', 'routineManager', routinesController]);

    function routinesController($scope, routineManager) {
        $scope.state = "routines";
        $scope.days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        $scope.showAddRoutineForm = function (day) {
            $scope.clickedDay = day;
            $scope.showAddRoutine = true;
            $scope.showroutine = false;
            $scope.activityTitle = "";
            $scope.activityDetails = "";
            var startTimeControl = document.querySelector("#startTime");
            var endTimeControl = document.querySelector("#endTime");
            // Note, JavaScript months are 0 based so September is referenced by 8, not 9
            var initialDate = new Date(2016, 5, 5, 12, 0, 0, 0);
            var startcontrol = new WinJS.UI.TimePicker(startTimeControl, { current: initialDate });
            var finishcontrol = new WinJS.UI.TimePicker(endTimeControl, { current: initialDate });
        }
        $scope.showDayRoutine = function (day) {
            $scope.showroutine = true;
            $scope.showAddRoutine = false;
            $scope.clickedDay = day;
        }
        $scope.saveActivity = function (day, title, details) {
            var startTimeControl = document.querySelector("#startTime");
            var endTimeControl = document.querySelector("#endTime");
            routineManager.saveActivity(day, title, details, startTimeControl.winControl._currentTime, endTimeControl.winControl._currentTime, $scope.routine.jsonData);
            $scope.showroutine = true;
            $scope.showAddRoutine = false;
        }
    }
}());
