﻿(function () {
    'use strict';

    timeSync.controller("helpController", ['$scope', helpController]);

    function helpController($scope) {
        $scope.state = "help";
    }

}());