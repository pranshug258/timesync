﻿(function () {
    'use strict';

    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;
    app.onactivated = function (args) {
        if (args.detail.kind === activation.ActivationKind.launch) {
            if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated) {
                // TODO: This application has been newly launched. Initialize your application here.
                //var json = JSON.parse('{ "Monday" : {"activities" : [{"title": "Hello","details": "Hello details","start": "4:50","finish": "5:20"}]},"Tuesday" : {"activities" : [{"title": "Hello","details": "Hello details","start": "4:50","finish": "5:20"}]},"Wednesday" : {"activities" : [{"title": "Hello","details": "Hello details","start": "4:50","finish": "5:20"}]},"Thursday" : {"activities" : [{"title": "Hello","details": "Hello details","start": "4:50","finish": "5:20"}]},"Friday" : {"activities" : [{"title": "Hello","details": "Hello details","start": "4:50","finish": "5:20"}]},"Saturday" : {"activities" : [{"title": "Hello","details": "Hello details","start": "4:50","finish": "5:20"}]},"Sunday" : {"activities" : [{"title": "Hello","details": "Hello details","start": "4:50","finish": "5:20"}]}}')
                //var jsontext = JSON.stringify(json)
                //console.log(jsontext)
                //saveData(jsontext);

            } else {
                // TODO: This application has been reactivated from suspension.
                // Restore application state here.
            }
            args.setPromise(WinJS.UI.processAll().then(function () {
                // TODO: Your code here.
                var titleBar = Windows.UI.ViewManagement.ApplicationView.getForCurrentView().titleBar;
                titleBar.backgroundColor = Windows.UI.Colors.firebrick;
                titleBar.foregroundColor = Windows.UI.Colors.white;
                titleBar.buttonBackgroundColor = Windows.UI.Colors.firebrick;
                titleBar.buttonForegroundColor = Windows.UI.Colors.white;
                titleBar.buttonHoverBackgroundColor = Windows.UI.Colors.darkRed;
                titleBar.buttonHoverForegroundColor = Windows.UI.Colors.white;
                var splitView = document.querySelector(".splitView").winControl;
                new WinJS.UI._WinKeyboard(splitView.paneElement); // Temporary workaround: Draw keyboard focus visuals on NavBarCommands
                
            }));
        }
    };
    app.oncheckpoint = function (args) {
        // TODO: This application is about to be suspended. Save any state that needs to persist across suspensions here.
        // You might use the WinJS.Application.sessionState object, which is automatically saved and restored across suspension.
        // If you need to complete an asynchronous operation before your application is suspended, call args.setPromise().
    };

    app.start();
}());