﻿(function () {
    'use strict';

    timeSync.service("dataService", [dataService]);

    var applicationData = Windows.Storage.ApplicationData.current;
    var localFolder = applicationData.localFolder;
    var saveData = function (obj) {
        return localFolder.createFileAsync("routine.json", Windows.Storage.CreationCollisionOption.replaceExisting).then(function (sampleFile) {
            return Windows.Storage.FileIO.writeTextAsync(sampleFile, obj.toString());
        })
    }

    function dataService() {
        this.getDay = function () {
            var dt = new Date()
            var day = dt.getDay()
            return day;
        }

        this.saveActivity = function (day, title, details, s, f, routine) {
            //console.log(day, title, details);
            //console.log(s.getHours(), ':', s.getMinutes(), ", ", f.getHours(), ':', f.getMinutes());
            console.log(routine);
            
            var activity = {};
            activity.title = title;
            activity.details = details;
            activity.start = s.getHours() + ':' + s.getMinutes();
            activity.finish = f.getHours() + ':' + f.getMinutes();

            routine[day].activities.push(activity);
            saveData(JSON.stringify(routine));
            console.log(routine);

        }
    }
}());