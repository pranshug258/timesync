﻿(function () {
    'use strict';

    timeSync.service("homeManager", ['dataService', homeManager]);

    function homeManager(dataService) {
        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
        this.getDay = function () {
            return days[dataService.getDay()];
        }
    }
}());