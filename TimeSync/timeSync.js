﻿'use strict';

var timeSync = angular.module("timeSync", ['ngRoute', 'ngAnimate']);

timeSync.config(function ($routeProvider) {
    $routeProvider
        // route for the home page
        .when('/', {
            templateUrl: 'pages/home.html',
            controller: 'homeController'
        })
        .when('/routines', {
            templateUrl: 'pages/routines.html',
            controller: 'routinesController'
        })
        .when('/help', {
            templateUrl: 'pages/help.html',
            controller: 'helpController'
        })
});